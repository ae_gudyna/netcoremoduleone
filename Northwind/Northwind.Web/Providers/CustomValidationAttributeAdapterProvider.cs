﻿ using System;
using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using System.Linq;
using System.Threading.Tasks;
 using Microsoft.AspNetCore.Mvc.DataAnnotations;
 using Microsoft.Extensions.Localization;
 using NorthWind.Web.Attributes.Validation;

 namespace NorthWind.Web.Providers
{
    public class CustomValidationAttributeAdapterProvider : IValidationAttributeAdapterProvider
    {
        private readonly IValidationAttributeAdapterProvider baseProvider =
            new ValidationAttributeAdapterProvider();

        public IAttributeAdapter GetAttributeAdapter(ValidationAttribute attribute,
            IStringLocalizer stringLocalizer)
        {
            if (attribute is QuantityPerUnitAttribute unitAttribute)
            {
                return new QuantityPerUnitAttributeAdapter(unitAttribute, stringLocalizer);
            }
            if (attribute is UnitsInStockAttribute countAttribute)
            {
                return new UnitsInStockAttributeAdapter(countAttribute, stringLocalizer);
            }

            return baseProvider.GetAttributeAdapter(attribute, stringLocalizer);
        }
    }
}
