﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using NorthWind.BL.Interfaces;
using NorthWind.DAL;
using NorthWind.Web.Mapping;
using NorthWind.Web.Models.Products;

namespace NorthWind.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService productService;
        private readonly ICategoryService categoryService;
        private readonly ISupplierService supplierService;
        private readonly IConfiguration configuration;

        public ProductController(
            IProductService productService, 
            ICategoryService categoryService, 
            ISupplierService supplierService, 
            IConfiguration configuration)
        {
            this.productService = productService;
            this.supplierService = supplierService;
            this.categoryService = categoryService;
            this.configuration = configuration;
        }

        public async Task<IActionResult> Index()
        {
            var maxProductsOnPageString = configuration["Constants:MaxProductsOnPage"];
            int.TryParse(maxProductsOnPageString, out var maxProductsOnPage);
            return View(await productService.GetListOfProductsAsync(maxProductsOnPage));
        }

        public async Task<IActionResult> Create()
        {
            ViewData["CategoryId"] = new SelectList(await categoryService.GetCategories(), "CategoryId", "CategoryName");
            ViewData["SupplierId"] = new SelectList(await supplierService.GetSuppliers(), "SupplierId", "CompanyName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateProductModel productModel)
        {
            if (ModelState.IsValid)
            {
                var product = productModel.MapToProduct();
                await productService.CreateProductAsync(product);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await categoryService.GetCategories(), "CategoryId", "CategoryName", productModel.CategoryId);
            ViewData["SupplierId"] = new SelectList(await supplierService.GetSuppliers(), "SupplierId", "CompanyName", productModel.SupplierId);
            return View(productModel);
        }
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productService.GetProductAsync(id.Value);
            if (product == null)
            {
                return NotFound();
            }
            var productModel = product.MapToUpdateModel();

            ViewData["CategoryId"] = new SelectList(await categoryService.GetCategories(), "CategoryId", "CategoryName", product.CategoryId);
            ViewData["SupplierId"] = new SelectList(await supplierService.GetSuppliers(), "SupplierId", "CompanyName", product.SupplierId);
            
            return View(productModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, UpdateProductModel productModel)
        {
            if (id != productModel.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var product = productModel.MapToProduct();
                await productService.UpdateProductAsync(product);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await categoryService.GetCategories(), "CategoryId", "CategoryName", productModel.CategoryId);
            ViewData["SupplierId"] = new SelectList(await supplierService.GetSuppliers(), "SupplierId", "CompanyName", productModel.SupplierId);
            return View(productModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await productService.DeleteProductAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
