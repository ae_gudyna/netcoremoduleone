﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NorthWind.Core.Exceptions;

namespace NorthWind.Web.Handlers
{
    public static class ErrorHandler
    {
        public static IApplicationBuilder UseErrorHandler(this IApplicationBuilder app)
        {
            return app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var error = context.Features.Get<IExceptionHandlerPathFeature>()?.Error;
                    var logger = errorApp.ApplicationServices.GetRequiredService<ILogger<Program>>();
                    var guid = "";

                    if (error != null)
                    {
                        guid = Guid.NewGuid().ToString();
                        logger.LogError(error, $"error occured with guid {guid}");
                    }
                    context.Response.Redirect($"/Home/Error/{guid}");
                });
            });
        }
    }
}
