﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorthWind.DAL.Entities;
using NorthWind.Web.Models.Products;

namespace NorthWind.Web.Mapping
{
    public static class ProductMapping
    {
        public static Products MapToProduct(this UpdateProductModel productModel)
        {
            return new Products
            {
                CategoryId = productModel.CategoryId,
                ProductId = productModel.ProductId,
                Discontinued = productModel.Discontinued,
                ProductName = productModel.ProductName,
                QuantityPerUnit = productModel.QuantityPerUnit,
                ReorderLevel = productModel.ReorderLevel,
                SupplierId = productModel.SupplierId,
                UnitPrice = productModel.UnitPrice,
                UnitsInStock = productModel.UnitsInStock,
                UnitsOnOrder = productModel.UnitsOnOrder
            };
        }

        public static Products MapToProduct(this CreateProductModel productModel)
        {
            return new Products
            {
                CategoryId = productModel.CategoryId,
                Discontinued = productModel.Discontinued,
                ProductName = productModel.ProductName,
                QuantityPerUnit = productModel.QuantityPerUnit,
                ReorderLevel = productModel.ReorderLevel,
                SupplierId = productModel.SupplierId,
                UnitPrice = productModel.UnitPrice,
                UnitsInStock = productModel.UnitsInStock,
                UnitsOnOrder = productModel.UnitsOnOrder
            };
        }


        public static UpdateProductModel MapToUpdateModel(this Products product)
        {
            return new UpdateProductModel
            {
                CategoryId = product.CategoryId,
                ProductId = product.ProductId,
                Discontinued = product.Discontinued,
                ProductName = product.ProductName,
                QuantityPerUnit = product.QuantityPerUnit,
                ReorderLevel = product.ReorderLevel,
                SupplierId = product.SupplierId,
                UnitPrice = product.UnitPrice,
                UnitsInStock = product.UnitsInStock,
                UnitsOnOrder = product.UnitsOnOrder
            };
        }
    }
}
