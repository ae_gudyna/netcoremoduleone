﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NorthWind.Web.Attributes.Validation;

namespace NorthWind.Web.Models.Products
{
    public class CreateProductModel
    {
        [Display(Name = "Name")]
        [Required]
        public string ProductName { get; set; }
        [Display(Name = "Supplier")]
        public int? SupplierId { get; set; }
        [Display(Name = "Category")]
        public int? CategoryId { get; set; }
        [Display(Name = "Quantity Per Unit")]
        [Required]
        public string QuantityPerUnit { get; set; }
        [Display(Name = "Unit Price")]
        [NonNegativeDecimal]
        public decimal? UnitPrice { get; set; }
        [Display(Name = "Units In Stock")]
        [NonNegativeNumber]
        public short? UnitsInStock { get; set; }
        [Display(Name = "Units On Order")]
        [NonNegativeNumber]
        public short? UnitsOnOrder { get; set; }
        [Display(Name = "Reorder Level")]
        [Required]
        public short? ReorderLevel { get; set; }
        [Display(Name = "Discontinued")]
        public bool Discontinued { get; set; }
    }
}
