﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;

namespace NorthWind.Web.Attributes.Validation
{
    public class NonNegativeNumberAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null && value is short number)
            {
                if (number >= 0)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult("Please enter non negative value.");
            }
            return new ValidationResult("" + validationContext.DisplayName + " has incorrect value");
        }
    }
}
