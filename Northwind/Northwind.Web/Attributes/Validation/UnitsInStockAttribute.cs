﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Localization;
using NorthWind.Web.Models.Products;

namespace NorthWind.Web.Attributes.Validation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class UnitsInStockAttribute : ValidationAttribute, IClientModelValidator
    {
        public int MaxCount;
        public UnitsInStockAttribute(int maxCount)
        {
            MaxCount = maxCount;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is short count)
            {
                if (count >= MaxCount)
                {
                    return new ValidationResult(GetErrorMessage());
                }

                return ValidationResult.Success;
            }

            return new ValidationResult("incorrect type of field");
        }

        internal string GetErrorMessage()
        {
            return $"Units should be not more than {MaxCount}";
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            MergeAttribute(context.Attributes, "data-val", "true");
            var errorMessage = FormatErrorMessage(context.ModelMetadata.GetDisplayName());
            MergeAttribute(context.Attributes, "data-val-unitsinstock", errorMessage);
            
            MergeAttribute(context.Attributes, "data-val-unitsinstock-count", MaxCount.ToString(CultureInfo.InvariantCulture));
        
    }

        private bool MergeAttribute(
            IDictionary<string, string> attributes,
            string key,
            string value)
        {
            if (attributes.ContainsKey(key))
            {
                return false;
            }
            attributes.Add(key, value);
            return true;
        }
    }
}
