﻿using Microsoft.AspNetCore.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Localization;

namespace NorthWind.Web.Attributes.Validation
{
    public class UnitsInStockAttributeAdapter : AttributeAdapterBase<UnitsInStockAttribute>
    {
        public UnitsInStockAttributeAdapter(UnitsInStockAttribute attribute,
            IStringLocalizer stringLocalizer)
            : base(attribute, stringLocalizer)
        {

        }
        public override void AddValidation(ClientModelValidationContext context)
        {
            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, "data-val-unitsinstock", GetErrorMessage(context));
            
            var count = Attribute.MaxCount.ToString(CultureInfo.InvariantCulture);
            MergeAttribute(context.Attributes, "data-val-unitsinstock-count", count);
        }

        public override string GetErrorMessage(ModelValidationContextBase validationContext) =>
            Attribute.GetErrorMessage();

    }
}
