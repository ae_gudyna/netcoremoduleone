﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NorthWind.Web.Attributes.Validation
{
    public class QuantityPerUnitAttribute : ValidationAttribute
    {
        private readonly List<string> Containers = new List<string>{"box", "bottle", "jar", "pkg", "glass", "can", "piece", "sausg", "tin", "bar"};
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null && value is string str)
            {
                if (Containers.Any(x => str.Contains(x)))
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult(GetErrorMessage());
            }
            return new ValidationResult("" + validationContext.DisplayName + " has incorrect value");
        }

        internal string GetErrorMessage()
        {
            return "Please enter volume. For example, boxes, pieces and so on";
        }
    }
}
