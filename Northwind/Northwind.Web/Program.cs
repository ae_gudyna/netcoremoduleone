using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace NorthWind.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            var logger = host.Services.GetRequiredService<ILogger<Program>>();
            logger.LogInformation("Start of the NorthWind App");
            var config = host.Services.GetRequiredService<IConfiguration>();

            logger.LogInformation("Configuration");
            foreach (var section in config.GetChildren())
            {
                
                LogChildren(section, logger);
            }
            host.Run();
        }

        static void LogChildren(IConfigurationSection config, ILogger<Program> logger, string spacing = "")
        {
            var children = config.GetChildren();
            if (children.Any())
            {
                logger.LogInformation(spacing + config.Key + ":");
                spacing += "-";
                foreach (var section in config.GetChildren())
                {
                    LogChildren(section, logger, spacing);
                }
            }
            else
            {
                logger.LogInformation(spacing + config.Key + ": " + config.Value);
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.AddSerilog();
                    logging.AddDebug();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog((hostingContext, loggerConfiguration) => loggerConfiguration
                    .ReadFrom.Configuration(hostingContext.Configuration));
    }
}
