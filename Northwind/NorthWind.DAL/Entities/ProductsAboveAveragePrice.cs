﻿using System;
using System.Collections.Generic;

namespace NorthWind.DAL.Entities
{
    public partial class ProductsAboveAveragePrice
    {
        public string ProductName { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}
