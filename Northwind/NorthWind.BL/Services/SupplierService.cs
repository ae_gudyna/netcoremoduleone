﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NorthWind.BL.Interfaces;
using NorthWind.DAL;
using NorthWind.DAL.Entities;

namespace NorthWind.BL.Services
{
    public class SupplierService : ISupplierService
    {
        private readonly NorthWindContext dbContext;

        public SupplierService(NorthWindContext db)
        {
            dbContext = db;
        }
        public async Task<IEnumerable<Suppliers>> GetSuppliers()
        {
            return await dbContext.Suppliers.ToListAsync();
        }
    }
}
