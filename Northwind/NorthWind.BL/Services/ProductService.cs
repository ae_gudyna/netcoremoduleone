﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NorthWind.BL.Interfaces;
using NorthWind.Core.Exceptions;
using NorthWind.DAL;
using NorthWind.DAL.Entities;

namespace NorthWind.BL.Services
{
    public class ProductService : IProductService
    {
        private readonly NorthWindContext _context;
        private readonly ILogger<ProductService> logger;

        public ProductService(NorthWindContext context, ILogger<ProductService> logger)
        {
            _context = context;
            this.logger = logger;
        }

        public async Task<bool> CreateProductAsync(Products product)
        {
            _context.Add(product);
            await _context.SaveChangesAsync();
            
            return true;
        }

        public async Task<bool> DeleteProductAsync(int id)
        {
            var products = await _context.Products.FindAsync(id);
            if (products == null)
            {
                throw new EntityNotFoundException(nameof(Products), id);
            }
            _context.Products.Remove(products);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<Products>> GetListOfProductsAsync(int numberOnPage, int pageNumber = 1)
        {
            var products = _context.Products
                .Include(p => p.Category)
                .Include(p => p.Supplier)
                .AsQueryable();
            
            if (pageNumber != 0)
            {
                products = products.Skip(pageNumber-1);
            }
            
            if (numberOnPage != 0)
            {
                products = products.Take(numberOnPage);
            }

            return await products.ToListAsync();
        }

        public async Task<Products> GetProductAsync(int productId)
        {
            var product = await _context.Products.FindAsync(productId);
            if (product == null)
            {
                throw new EntityNotFoundException(nameof(Products), productId);
            }

            return product;
        }

        public async Task<bool> UpdateProductAsync(Products product)
        {
            _context.Update(product);
            await _context.SaveChangesAsync();
            
            return true;
        }
    }
}
