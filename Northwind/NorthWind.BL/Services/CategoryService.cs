﻿using NorthWind.BL.Interfaces;
using NorthWind.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NorthWind.DAL;

namespace NorthWind.BL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly NorthWindContext dbContext;

        public CategoryService(NorthWindContext db)
        {
            dbContext = db;
        }

        public async Task<IEnumerable<Categories>> GetCategories()
        {
            return await dbContext.Categories.ToListAsync();
        }
    }
}
