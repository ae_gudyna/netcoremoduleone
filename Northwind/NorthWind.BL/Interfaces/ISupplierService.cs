﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NorthWind.DAL.Entities;

namespace NorthWind.BL.Interfaces
{
    public interface ISupplierService
    {
        Task<IEnumerable<Suppliers>> GetSuppliers();
    }
}
