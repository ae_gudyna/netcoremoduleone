﻿using System.Collections.Generic;
using NorthWind.DAL.Entities;
using System.Threading.Tasks;

namespace NorthWind.BL.Interfaces
{
    public interface ICategoryService
    {
        Task<IEnumerable<Categories>> GetCategories();
    }
}
