﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NorthWind.DAL.Entities;

namespace NorthWind.BL.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<Products>> GetListOfProductsAsync(int numberOnPage, int pageNumber = 1);
        Task<Products> GetProductAsync(int productId);
        Task<bool> UpdateProductAsync(Products product);
        Task<bool> CreateProductAsync(Products product);
        Task<bool> DeleteProductAsync(int id);
    }
}
