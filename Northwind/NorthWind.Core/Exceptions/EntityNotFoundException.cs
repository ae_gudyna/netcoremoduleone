﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWind.Core.Exceptions
{
    public class EntityNotFoundException : ApplicationException
    {
        public EntityNotFoundException(string entityName) 
            : base($"Entity {entityName} was not found")
        {

        }

        public EntityNotFoundException(string entityName, int entityId)
            :base($"Entity {entityName} with id {entityId} was not found")
        {
                
        }
    }
}
